var gulp = require('gulp');
var uncss = require('gulp-uncss');

gulp.task('default', function () {
    return gulp.src('css/style.css')
        .pipe(uncss({
            html: ['index.html', 'catalog.html']
        }))
        .pipe(gulp.dest('css/mini'));
});
