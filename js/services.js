// Services
// Find elements
let servicesButtons = document.querySelectorAll('.services-slider__button')
let servicesDescs = document.querySelectorAll('.services-slider__description')

// Event
servicesButtons.forEach(servicesClick);

// Functions
// Click to tab (button) services
function servicesClick(button) {
  button.addEventListener('click', function () {
    let currentButton = button;
    let descId = currentButton.getAttribute("data-desc");
    let currentDesc = document.querySelector(descId)

    if ( ! button.classList.contains('services-slider__button_active')) {
      servicesButtons.forEach(function (button) {
        button.classList.remove('services-slider__button_active')
      });

      currentButton.classList.add('services-slider__button_active')

      servicesDescs.forEach(function (desc) {
        desc.classList.remove('services-slider__description_active')
      });

      currentDesc.classList.add('services-slider__description_active')
    };
  });
};
