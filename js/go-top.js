// "Go top" button

// Find element
let goTopButton = document.querySelector('.go-top')

// Event
window.addEventListener('scroll', function () {
  let scrollPos = window.scrollY;
  if (scrollPos > 300) {
    // Show button when scrolled 300 pixels from top
    goTopButton.classList.add('go-top_active');
  } else {
    // Hide button when scrolling 300px from top
    goTopButton.classList.remove('go-top_active');
  }
});
