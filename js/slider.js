// Find elements
let slides = document.querySelectorAll('.slider__slide');
let prevButton = document.querySelector('.slider__button_left');
let nextButton = document.querySelector('.slider__button_right');
let bullets = document.querySelectorAll('.slider__bullet');

// Events
nextButton.addEventListener('click', clickNextButton);
prevButton.addEventListener('click', clickPrevButton);

// Assigning an index
let currentIndex = 0;
let activeSlide = slides[currentIndex];
let activeBullet = bullets[currentIndex];

// Functions
// Click to prev slide button
function clickPrevButton() {
  prevButton.disabled = true;
  nextButton.disabled = true;

  if (currentIndex == 0) {
    currentIndex = slides.length - 1;
  } else {
    currentIndex--
  };

  slideChanges();

  slides.forEach(function (slideShow) {
    if (slideShow == slides[currentIndex]) {
      activeSlide.classList.add('slide-show_prev')
    } else {
      slideShow.classList.add('slider__slide_absolute')
      window.setTimeout(function() {
        slideShow.classList.remove('slider__slide_active');
        slideShow.classList.remove('slider__slide_absolute')
      }, 1200)
    }
  });

  window.setTimeout(function() {
    activeSlide.classList.remove('slide-show_prev')
    prevButton.disabled = false;
    nextButton.disabled = false;
  }, 1200)

  activeBullet.classList.add('slider__bullet_active');
};

// Click to next slide button
function clickNextButton() {
  prevButton.disabled = true;
  nextButton.disabled = true;

  if (currentIndex == slides.length - 1) {
    currentIndex = 0;
  } else {
    currentIndex++
  };

  slideChanges();

  slides.forEach(function (slideShow) {
    if (slideShow == slides[currentIndex]) {
      activeSlide.classList.add('slide-show_next')
    } else {
      slideShow.classList.add('slider__slide_absolute')
      window.setTimeout(function() {
        slideShow.classList.remove('slider__slide_active');
        slideShow.classList.remove('slider__slide_absolute')
      }, 1200)
    }
  });

  window.setTimeout(function() {
    activeSlide.classList.remove('slide-show_next')
    prevButton.disabled = false;
    nextButton.disabled = false;
  }, 1200)

  activeBullet.classList.add('slider__bullet_active');
};

// Part of the function that changes slides
function slideChanges() {
  activeSlide = slides[currentIndex];
  activeBullet = bullets[currentIndex];

  bullets.forEach(function (bulletActive) {
    bulletActive.classList.remove('slider__bullet_active');
  });

  activeSlide.classList.add('slider__slide_active');
};
