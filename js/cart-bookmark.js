// Cart (+modal window) and bookmark
// Find elements
let headerBookmark = document.querySelector('.header-top__bookmark');
let headerCart = document.querySelector('.header-top__cart');
let modalCart = document.querySelector('.modal-cart'); // Modal
let modalCartClose = document.querySelector('.modals__close_cart');
let cartCloseButton = document.querySelector('.modal-cart__botton_white'); // continue button to close
let cardBookmark = document.querySelectorAll('.goods-list__bookmark'); // Product card button
let cardBuy = document.querySelectorAll('.goods-list__buy'); // Product card button

// Events
// Click "to bookmarks" on the product card
cardBookmark.forEach(function (entry) {
  entry.addEventListener("click", function() {
    if ( ! headerBookmark.classList.contains('header-top__bookmark_active')) {
      headerBookmark.classList.add('header-top__bookmark_active');
    }
  });
});

// Click "buy" on the product card
cardBuy.forEach(function (entry) {
  entry.addEventListener("click", function() {
    modalCart.classList.add('modal-cart_active');
    if ( ! headerCart.classList.contains('header-top__cart_active')) {
      headerCart.classList.add('header-top__cart_active');
    }
  });
});

// Modal cart
modalCartClose.addEventListener("click", function(evt) {
  evt.preventDefault();
  modalCart.classList.remove('modal-cart_active');
});

cartCloseButton.addEventListener("click", function(evt) {
  evt.preventDefault();
  modalCart.classList.remove('modal-cart_active');
});

// Сlose by keyboard
window.addEventListener('keydown', function (evt) {
  if (evt.keyCode === 27) {
    if (modalCart.classList.contains('modal-cart_active')) {
      evt.preventDefault();
      modalCart.classList.remove('modal-cart_active');
    };
  };
});
