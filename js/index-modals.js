// Find elements
// Modal map
let mapButton = document.querySelector('.about-contacts__img-button');
let modalMap = document.querySelector('.modal-map');
let modalMapClose = document.querySelector('.modals__close_map');
// 'Write us' button and modal
let writeUsButton = document.querySelector('.about-contacts__write-button');
let modalWriteClose = document.querySelector('.modals__close_write');
let modalWrite = document.querySelector('.write-us');
  let writeName = modalWrite.querySelector('[name=name]');
  let writeEmail = modalWrite.querySelector(['[name=email]']);
  let writeText = modalWrite.querySelector(['[name=text]']);
let writeForm = document.querySelector('.write-us__form');
let inputFields = [writeName, writeEmail, writeText];

// Set storage
let isStorageSupport = true;
let storageName = localStorage.getItem('writeName');

// Modal write us
modalWriteClose.addEventListener('click', function () {
  modalWrite.classList.remove('modals_active');
  modalWrite.classList.remove('modals_error');
});

try {
  storageName = localStorage.getItem('writeName');
} catch (err) {
  isStorageSupport = false;
}

writeUsButton.addEventListener('click', function () {
  modalWrite.classList.add('modals_active');
  writeName.value = localStorage.getItem('storageName');
  if (writeName.value) {
    writeEmail.focus();
  } else {
    writeName.focus();
  }
});

// Validation
writeForm.addEventListener('submit', function (evt) {
  if ( ! writeName.value || ! writeEmail.value || ! writeText.value) {
    evt.preventDefault();
    modalWrite.classList.remove('modals_error');
    modalWrite.offsetWidth = modalWrite.offsetWidth;
    modalWrite.classList.add('modals_error');

    let inputs = [writeName, writeEmail, writeText];

    if ( ! writeName.value) {
      writeName.classList.add('write-us__input_error');
    };
    writeName.addEventListener('input', function (evt) {
      evt.preventDefault();
      writeName.classList.remove('write-us__input_error');
    });


    if ( ! writeEmail.value) {
      writeEmail.classList.add('write-us__input_error');
    };
    writeEmail.addEventListener('input', function (evt) {
      evt.preventDefault();
      writeEmail.classList.remove('write-us__input_error');
    });


    if ( ! writeText.value) {
      writeText.classList.add('write-us__input_error');
    };
    writeText.addEventListener('input', function (evt) {
      evt.preventDefault();
      writeText.classList.remove('write-us__input_error');
    });

  } else {
    if (isStorageSupport) {
      localStorage.setItem('storageName', writeName.value);
    }
  }
});

// Сlose by keyboard
window.addEventListener('keydown', function (evt) {
  if (evt.keyCode === 27) {
    if (modalWrite.classList.contains('modals_active')) {
      evt.preventDefault();
      modalWrite.classList.remove('modals_active');
      modalWrite.classList.remove('modals_error');
    } else if (modalMap.classList.contains('modals_active')) {
      evt.preventDefault();
      modalMap.classList.remove('modals_active');
    };
  };
});

// Modal map
mapButton.addEventListener('click', function () {
  modalMap.classList.add('modals_active');
});

modalMapClose.addEventListener('click', function () {
  modalMap.classList.remove('modals_active');
});
